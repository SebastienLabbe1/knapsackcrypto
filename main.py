#!/bin/python3
from sage.all import *
import random as rd

def elemWise(o, A, B):
    print(A.parent(), B.parent())
    C = matrix(A)
    for i in range(A.nrows()):
        for j in range(A.ncols()):
            C[i,j] = o(A[i,j], B[i,j])
    return A

def printKeys(v,S,k,p,d,M):
    print("public key")
    print(f"v = {v} \nS = {S} \nk = {k}")
    print(f"p = {p} \nd = {d} \nM = {M}")

def testCondition(d,v,p,n,t):
    for i in range(n):
        for j in range(t):
            if n * ((d[0,j] * v[0,i]) % p[0,j]) >= p[0,j]:
                print("Condition not met")
                print(f"dj = {d[0,j]} \nvi = {v[0,i]} \npj = {p[0,j]} n = {n}")
                print(n * ((d[0,j] * v[0,i]) % p[0,j]) < p[0,j])
    print("Condition check finished")

def genVRandom(n):
    return random_matrix(ZZ,1,n)

def genVBrute(d,p,n,t):
    v = random_matrix(ZZ,1,n)
    for i in range(n):
        while True:
            v[0,i] = randrange(0,1<<n)
            for j in range(t):
                if n * ((d[0,j] * v[0,i]) % p[0,j]) >= p[0,j]:
                    break
            else:
                break
    return v

def genVImproved(n):
    pass

n = 3
t = 2
k = 256

p = matrix([random_prime((2<<k)-1,False,(2<<(k-1))) for i in range(t)])
d = random_matrix(ZZ,1,t)
M = random_matrix(ZZ,t,t) % 2

v = genVRandom(n) #implement genVImproved
v = genVBrute(d,p,n,t)

w = matrix([[(d[0,j]*v[0,i])%p[0,j] for j in range(t)] for i in range(n)])
S = (w * M) % (2 << k)

printKeys(v,S,k,p,d,M)
testCondition(d,v,p,n,t)


